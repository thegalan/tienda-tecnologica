package com.ceiba.tiendatecnologica.aplicacion.fabrica;

import com.ceiba.tiendatecnologica.aplicacion.comando.ComandoProducto;
import com.ceiba.tiendatecnologica.dominio.CalculoDeFechasGarantiaExtendida;
import com.ceiba.tiendatecnologica.dominio.CalculoPrecioGarantiaExtendida;
import com.ceiba.tiendatecnologica.dominio.GarantiaExtendida;
import com.ceiba.tiendatecnologica.dominio.Producto;
import org.springframework.stereotype.Component;

@Component
public class FabricaGarantia {

    CalculoPrecioGarantiaExtendida calculoPrecioGarantiaExtendida = new CalculoPrecioGarantiaExtendida();
    CalculoDeFechasGarantiaExtendida calculoDeFechasGarantiaExtendida = new CalculoDeFechasGarantiaExtendida();

    public GarantiaExtendida crearGarantia(ComandoProducto comandoProducto, String nombreCliente){
        return new GarantiaExtendida(
                new Producto(comandoProducto.getId(), comandoProducto.getCodigo(), comandoProducto.getNombre(), comandoProducto.getPrecio()),
                calculoDeFechasGarantiaExtendida.calcularFechaInicioGrtia(),
                calculoDeFechasGarantiaExtendida.calcularFechaFinGrtia(comandoProducto.getPrecio()),
                calculoPrecioGarantiaExtendida.calcularPrecioGarantia(comandoProducto.getPrecio()),
                nombreCliente);
    }
}
