package com.ceiba.tiendatecnologica.aplicacion.manejadores.garantia;

import com.ceiba.tiendatecnologica.aplicacion.comando.ComandoProducto;
import com.ceiba.tiendatecnologica.aplicacion.fabrica.FabricaGarantia;
import com.ceiba.tiendatecnologica.dominio.GarantiaExtendida;
import com.ceiba.tiendatecnologica.dominio.excepcion.GarantiaExtendidaException;
import com.ceiba.tiendatecnologica.dominio.servicio.vendedor.ServicioVendedor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class ManejadorGenerarGarantia {

	private final FabricaGarantia fabricaGarantia;
	private final ServicioVendedor servicioVendedor;


	public ManejadorGenerarGarantia(FabricaGarantia fabricaGarantia, ServicioVendedor servicioVendedor) {
		this.fabricaGarantia = fabricaGarantia;
		this.servicioVendedor = servicioVendedor;
	}
	
	@Transactional
	public GarantiaExtendida ejecutar(String codigoProducto, String nombreCliente, ComandoProducto comandoProducto) {

		if (!this.servicioVendedor.tieneGarantia(codigoProducto)){
			if(this.obtenerCantidadVocales(codigoProducto)==3){
				try {
					throw new GarantiaExtendidaException("Este producto no cuenta con garantía extendida");
				} catch (GarantiaExtendidaException exception) {
					exception.printStackTrace();
				}
				return null;
			}
			else {
				GarantiaExtendida garantia = this.fabricaGarantia.crearGarantia(comandoProducto, nombreCliente);
				this.servicioVendedor.generarGarantia(garantia);
				return garantia;
			}
		}else{
			try{
				throw new GarantiaExtendidaException(servicioVendedor.EL_PRODUCTO_TIENE_GARANTIA);
			}catch(GarantiaExtendidaException exception){
				exception.printStackTrace();
			}
		}
		return null;
	}

	public int obtenerCantidadVocales(String c) {
		int count = 0;
		for(int i = 0; i<=c.length()-1;i++){
			if("AEIOUaeiou".indexOf(c.charAt(i)) != -1) count ++;
		}
		return count;
	}

}