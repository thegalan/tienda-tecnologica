package com.ceiba.tiendatecnologica.infraestructura.controllador;

import com.ceiba.tiendatecnologica.aplicacion.comando.ComandoProducto;
import com.ceiba.tiendatecnologica.aplicacion.manejadores.garantia.ManejadorGenerarGarantia;
import com.ceiba.tiendatecnologica.aplicacion.manejadores.garantia.ManejadorObtenerGarantia;
import com.ceiba.tiendatecnologica.dominio.GarantiaExtendida;
import com.ceiba.tiendatecnologica.dominio.excepcion.GarantiaExtendidaException;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/garantia")
public class ControladorGarantia {
	private final ManejadorGenerarGarantia manejadorGenerarGarantia;
	private final ManejadorObtenerGarantia manejadorObtenerGarantia;

	public ControladorGarantia(ManejadorObtenerGarantia manejadorObtenerGarantia, ManejadorGenerarGarantia manejadorGenerarGarantia) {
		this.manejadorObtenerGarantia = manejadorObtenerGarantia;
		this.manejadorGenerarGarantia = manejadorGenerarGarantia;
	}

	@PostMapping("/{idProducto}/{nombreCliente}")
	public void generar(
			@PathVariable(name = "idProducto") String codigoProducto,
			@PathVariable(name = "nombreCliente") String nombreCliente,
			@RequestBody ComandoProducto comandoProducto) {
		this.manejadorGenerarGarantia.ejecutar(codigoProducto, nombreCliente, comandoProducto);
	}

	@GetMapping("/{id}")
	public GarantiaExtendida buscar(@PathVariable(name = "id") String codigo) {
		return this.manejadorObtenerGarantia.ejecutar(codigo);
	}
}
